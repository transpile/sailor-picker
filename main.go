package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"

	"os"
)

func main() {
	inkCount := 1
	if len(os.Args) > 1 {
		var err error
		inkCount, err = strconv.Atoi(os.Args[1])
		if err != nil {
			panic(err)
		}
	}

	rawBytes, err := os.ReadFile("available_inks.txt")
	if err != nil {
		panic(err)
	}

	inks := strings.Split(string(rawBytes), "\n")

	for i := 0; i < inkCount; i += 1 {
		n := rand.Intn(len(inks))
		fmt.Println(inks[n])
	}
}
